package com.movie.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.movie.test.config.openClearActivity

class SplashActivity : AppCompatActivity() {
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 1000 //3 seconds
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mDelayHandler = Handler()
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)


    }

    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {

            startMainActivity()
        }
    }

    override fun onResume() {
        super.onResume()
    }

    fun startMainActivity() {
        openClearActivity(MainActivity::class.java)
        finish()
       // overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );

    }



}