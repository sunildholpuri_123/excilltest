package com.movie.test.config

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


fun ImageView.loadImage(url: String) {
    Glide.with(this)
        .load(url)
        .diskCacheStrategy(DiskCacheStrategy.DATA)
        .into(this)
}


fun <T> Context.openClearActivity(it: Class<T>, extras: Bundle.() -> Unit = {}) {
    var intent = Intent(this, it)
    //intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    intent.putExtras(Bundle().apply(extras))
    startActivity(intent)


}

fun getDateWithServerTimeStamp(time:String): String? {
    val dateFormat = SimpleDateFormat(
        "yyyy-MM-dd'T'HH:mm:ss'Z'",
        Locale.getDefault()
    )
    val format2 = SimpleDateFormat("dd-MM-yyyy")

    val date: Date = dateFormat.parse(time)
    val outputDateStr = format2.format(date)


    try {
        return outputDateStr
    } catch (e: ParseException) {
        return null
    }

}