package com.movie.test.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.movie.test.MovieDetailActivity
import com.movie.test.R
import com.movie.test.adapter.MovieAdapter
import com.movie.test.config.openClearActivity
import com.movie.test.model.Search
import com.movie.test.repository.MainRepository
import com.movie.test.retrofit.RetrofitService
import com.movie.test.viewmodel.MainViewModel
import com.movie.test.viewmodel.MovieViewModel
import com.movie.test.viewmodel.MyViewModelFactory
import kotlinx.android.synthetic.main.fragment_movie.view.*


private const val ARG_PARAM1 = "param1"

class MovieFragment : Fragment(), MovieAdapter.onItemClcik {
    private var param1: Int? = null
    lateinit var movieViewModel: MovieViewModel

    lateinit var viewModel: MainViewModel
    private val TAG = "Movie"
    private val retrofitService = RetrofitService.getInstance()
    lateinit var adapter: MovieAdapter
    lateinit var root: View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1, 0)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_movie, container, false)
        init()
        setMovieAdapter()
        return root
    }


    fun setMovieAdapter() {

        adapter = MovieAdapter(activity!!, this)
        root.rvMovie.adapter = adapter
    }

    fun init() {
        movieViewModel = ViewModelProvider(this).get(MovieViewModel::class.java)
        viewModel =
            ViewModelProvider(this, MyViewModelFactory(MainRepository(retrofitService))).get(
                MainViewModel::class.java
            )
        if (param1 == 1) {
//        fetch data from server for Movie Tab
        fetchDataToServer()
        } else {
//          fetch data from DB to History Tab
           fetchDataToDB()
        }
    }

    private fun fetchDataToDB() {
        movieViewModel.getData(activity!!)!!.observe(activity!!, {
            if (it != null)
                adapter.setAppList(it as ArrayList<Search>)
        })
    }

    private fun fetchDataToServer() {
        viewModel.movieData.observe(activity!!, Observer {
            Log.d(TAG, "onCreate: $it")
            adapter.setAppList(it.Search as ArrayList<Search>)
        })

        viewModel.errorMessage.observe(activity!!, Observer {

        })
        viewModel.getMovieData()
    }


    companion object {
        @JvmStatic
        fun newInstance(param1: Int) =
            MovieFragment().apply {
                arguments = Bundle().apply {
                    putInt(
                        ARG_PARAM1, param1
                    )
                }
            }
    }


    override fun onDestroy() {
        super.onDestroy()
    }

    override fun Click(get: Search) {
        if (this::movieViewModel.isInitialized && movieViewModel != null) {
            movieViewModel.insertData(activity!!, get)
        }
        activity!!.openClearActivity(MovieDetailActivity::class.java)
        {
            putParcelable(activity!!.getString(R.string.detail), get)

        }
    }


}