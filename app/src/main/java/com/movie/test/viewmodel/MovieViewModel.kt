package com.movie.test.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.movie.test.model.Search
import com.movie.test.repository.MovieRepository

class MovieViewModel : ViewModel() {

    var movieData: LiveData<List<Search>>? = null
    fun insertData(context: Context,result: Search) {
        MovieRepository.insertData(context, result)
    }




    fun getData(context: Context): LiveData<List<Search>>? {
        movieData = MovieRepository.getData(context)
        return movieData
    }

}