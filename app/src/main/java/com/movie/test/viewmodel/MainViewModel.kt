package com.movie.test.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.movie.test.model.SearchResponse
import com.movie.test.repository.MainRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel constructor(private val repository: MainRepository)  : ViewModel() {

    val movieData = MutableLiveData<SearchResponse>()
    val errorMessage = MutableLiveData<String>()

    fun getMovieData() {

        val response = repository.getMovieData()
        response.enqueue(object : Callback<SearchResponse> {
            override fun onResponse(call: Call<SearchResponse>, response: Response<SearchResponse>) {
                movieData.postValue(response.body())
            }

            override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}