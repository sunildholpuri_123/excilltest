package com.movie.test.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "Search")
data class Search(
    val Poster: String,
    val Title: String,
    val Type: String,
    val Year: String,
    @PrimaryKey(autoGenerate = false)
    val imdbID: String
):Parcelable