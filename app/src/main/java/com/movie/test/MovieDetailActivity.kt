package com.movie.test

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.drm.*
import com.google.android.exoplayer2.source.dash.DashChunkSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.util.Util
import com.movie.test.model.Search
import kotlinx.android.synthetic.main.exo_playback_control_view.*
import kotlinx.android.synthetic.main.primary_movie_info.*
import java.util.*

class MovieDetailActivity : AppCompatActivity() {
    private var playerView: PlayerView? = null
    private var player: SimpleExoPlayer? = null
    private var trackSelector: DefaultTrackSelector? = null
    private var url: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.primary_movie_info)
        playerView = findViewById(R.id.playerView)
        intent?.getParcelableExtra<Search>(getString(R.string.detail)).let {
//            binding.
            getSupportActionBar()?.setTitle(it?.Title);
            tv_movie_title.text = it?.Title
            tv_release_date.text = it?.Year
            tv_user_rating.text = it?.imdbID
            header_tv.text = it?.Title

        }
        initializePlayer()
    }


    private fun initializePlayer() {

        url = "https://bitmovin-a.akamaihd.net/content/art-of-motion_drm/mpds/11331.mpd"
        var drmSessionManager: DefaultDrmSessionManager<FrameworkMediaCrypto?>? = null
        val drmLicenseUrl = "https://proxy.uat.widevine.com/proxy?provider=widevine_test"
        val drmSchemeUuid = Util.getDrmUuid(C.WIDEVINE_UUID.toString())


        try {
            drmSessionManager = buildDrmSessionManager(
                drmSchemeUuid, drmLicenseUrl, true
            )
        } catch (e: UnsupportedDrmException) {
            e.printStackTrace()
        }


        if (player == null) {
            trackSelector = DefaultTrackSelector()
//            trackSelector!!.setParameters(trackSelector!!.buildUponParameters().setMaxVideoSize(200, 200))
            player = ExoPlayerFactory.newSimpleInstance(
                applicationContext,
                trackSelector,
                DefaultLoadControl(),
                drmSessionManager
            )

            // Bind the player to the view.
            playerView!!.player = player

            player!!.playWhenReady = true
        }

        // Build the media item.
        val dashMediaSource = buildDashMediaSource(Uri.parse(url))

        // Prepare the player.
        player!!.prepare(dashMediaSource, true, false)
    }

    // Set the media item to be played.
    private fun buildDashMediaSource(uri: Uri): DashMediaSource {
        val userAgent = "ExoPlayer-Drm"
        val dashChunkSourceFactory: DashChunkSource.Factory = DefaultDashChunkSource.Factory(
            DefaultHttpDataSourceFactory("userAgent", DefaultBandwidthMeter())
        )
        val manifestDataSourceFactory = DefaultHttpDataSourceFactory(userAgent)
        return DashMediaSource.Factory(dashChunkSourceFactory, manifestDataSourceFactory)
            .createMediaSource(uri)
    }

    // Drm Manager
    @Throws(UnsupportedDrmException::class)
    private fun buildDrmSessionManager(
        uuid: UUID?, licenseUrl: String, multiSession: Boolean
    ): DefaultDrmSessionManager<FrameworkMediaCrypto?> {
        val licenseDataSourceFactory: HttpDataSource.Factory =
            DefaultHttpDataSourceFactory(Util.getUserAgent(this, application.packageName))
        val drmCallback = HttpMediaDrmCallback(licenseUrl, licenseDataSourceFactory)
        val mediaDrm = FrameworkMediaDrm.newInstance(uuid!!)
        return DefaultDrmSessionManager(uuid, mediaDrm, drmCallback, null, multiSession)
    }

    override fun onPause() {
        super.onPause()
        player!!.playWhenReady = false
    }
}