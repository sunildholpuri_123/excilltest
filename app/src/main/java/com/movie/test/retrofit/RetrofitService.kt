package com.movie.test.retrofit

import androidx.viewbinding.BuildConfig
import com.movie.test.model.SearchResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface RetrofitService {

    @GET
    fun getMovieData(
        @Url url:String,
        @Query("apikey") key:String,
                     @Query("s") search:String) : Call<SearchResponse>

    companion object {

        var retrofitService: RetrofitService? = null

        fun getInstance() : RetrofitService {

            if (retrofitService == null) {
                val levelType: HttpLoggingInterceptor.Level
                if (BuildConfig.BUILD_TYPE.contentEquals("debug"))
                    levelType = HttpLoggingInterceptor.Level.BODY else levelType = HttpLoggingInterceptor.Level.NONE

                val logging = HttpLoggingInterceptor()
                logging.setLevel(levelType)

                val okhttpClient = OkHttpClient.Builder()
                okhttpClient.addInterceptor(logging)
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://itunes.apple.com/")
                    .client(okhttpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }
}