package com.movie.test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.movie.test.adapter.ViewPagerAdapter
import com.movie.test.databinding.ActivityMainBinding
import com.movie.test.fragment.MovieFragment


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViewPager()

    }



    private fun setupViewPager() {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(MovieFragment.newInstance(1), getString(R.string.movie))
        adapter.addFragment(MovieFragment.newInstance(2), getString(R.string.history))
        binding.viewpager.adapter = adapter
        binding.tabs.setupWithViewPager(binding.viewpager)
    }



}