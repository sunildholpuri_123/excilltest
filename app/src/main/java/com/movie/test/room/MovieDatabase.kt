package com.test.nsdl.mvvm.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.movie.test.model.Search

@Database(
    entities = arrayOf(
        Search::class
    ), version = 1, exportSchema = false
)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun movieDao(): DAOAccess

    companion object {

        @Volatile
        private var INSTANCE: MovieDatabase? = null

        fun getDataseClient(context: Context): MovieDatabase {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, MovieDatabase::class.java, "MovieData")
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE!!

            }
        }

    }

}