package com.test.nsdl.mvvm.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.movie.test.model.Search

@Dao
interface DAOAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(storeModel: Search)

    @Query("SELECT * FROM Search")
    fun getData(): LiveData<List<Search>>



}