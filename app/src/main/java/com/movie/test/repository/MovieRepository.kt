package com.movie.test.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.movie.test.model.Search
import com.test.nsdl.mvvm.room.MovieDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class MovieRepository {

    companion object {

        var movieDatabase: MovieDatabase? = null

        var storeModel: LiveData<List<Search>>? = null

        fun initializeDB(context: Context): MovieDatabase {
            return MovieDatabase.getDataseClient(context)
        }

        fun insertData(context: Context, data:Search) {

            movieDatabase =
                initializeDB(
                    context
                )

            CoroutineScope(IO).launch {

                movieDatabase!!.movieDao().insertData(data)
            }

        }






        fun getData(context: Context): LiveData<List<Search>>? {

            movieDatabase =
                initializeDB(
                    context
                )

            storeModel = movieDatabase!!.movieDao().getData()

            return storeModel
        }

    }
}