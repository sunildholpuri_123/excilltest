package com.movie.test.repository

import com.movie.test.BuildConfig
import com.movie.test.Constants.Constants
import com.movie.test.retrofit.RetrofitService

class MainRepository constructor(private val retrofitService: RetrofitService) {
   // https://itunes.apple.com/search?term=Michael+jackson&amp;media=musicVideo
    fun getMovieData() = retrofitService.getMovieData(Constants.BaseUrl,BuildConfig.movie_api_key,Constants.SearchKey)
}