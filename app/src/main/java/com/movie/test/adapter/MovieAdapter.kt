package com.movie.test.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.movie.test.R
import com.movie.test.config.loadImage
import com.movie.test.model.Search
import kotlinx.android.synthetic.main.movie_layout.view.*


class MovieAdapter(private var context: Context,private var click: onItemClcik?) : RecyclerView.Adapter<MovieAdapter.MyViewHolder>() {
    private val movieList = ArrayList<Search>()
    fun setAppList(categoryModel: ArrayList<Search>) {
        movieList.clear()
        movieList.addAll(categoryModel)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        Log.d("LIST_SIZE", "" + movieList.size)
        return movieList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.name.text = movieList.get(position)?.Title
        holder.itemView.imageview.loadImage(movieList.get(position)?.Poster!!)
        holder.itemView.setOnClickListener {
            if(click!=null){
                click!!.Click(movieList.get(position))
            }



        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.movie_layout, parent, false)
        return MyViewHolder(
            itemView
        )
    }


    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
    interface onItemClcik {
        fun Click(get: Search)
    }
}